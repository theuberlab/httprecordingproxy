package main

import (
	"fmt"
	"io"
	"net/http"
	"time"

	"bitbucket.org/theuberlab/lug"
)


// A health handler which simply responds with a 200 OK
func healthHandler(w http.ResponseWriter, r *http.Request) {
	// Log the request to STDOUT
	lug.Warn("Message", "/health called at %s\n", time.Now())

	metricsArray.IncrementHealthCheckCounter(r.Method, "200")

	// Set CSS headers
	setCORS(w)

	// Respond with a 200
	w.WriteHeader(http.StatusOK)
	message := fmt.Sprintf("HEALTHY")
	io.WriteString(w, message)
}