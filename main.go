package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gopkg.in/alecthomas/kingpin.v2"

	"bitbucket.org/theuberlab/lug"
	"bitbucket.org/theuberlab/thewolf/config"
)

// General variables
var (
	// Byte arrays to store the private key and certificate we will use to serve TLS
	apiKeyPEM     []byte
	apiCertPEM    []byte
	fwdKeyPEM     []byte
	fwdCertPEM    []byte
	configFile    string
	configContext string
	logLevel      string
	logFormat     string
	logUTC        bool
	apiPort       int
	forwardPort   int
	appVersion    string
	//ctx           context.Context
	metricsArray  *VerifyMetrics
	configuration config.Configuration
)

// Variables that will be used as handles for specific activities.
var (
	// Creates the top level context for all commands flags and arguments
	app      			= kingpin.New("podstartupwatcher", "Connects to the kubernetes API and watches pod events while it spins up and destroys pods.")
	versionInfo			*VersionInfo
)

// I don't fully understand how this bit works but it's part of some cross-site scripting protection I got from a friend.
var corsHeaders = map[string]string{
	"Access-Control-Allow-Headers":  "Accept, Authorization, Content-Type, Origin",
	"Access-Control-Allow-Methods":  "GET, OPTIONS",
	"Access-Control-Allow-Origin":   "*",
	"Access-Control-Expose-Headers": "Date",
}

// Enables cross-site script calls.
func setCORS(w http.ResponseWriter) {
	for h, v := range corsHeaders {
		w.Header().Set(h, v)
	}
}

// Initialize some components that need to be available early on.
func init() {
	versionInfo = &VersionInfo{} // Doing this weird thing due to some legacy reasons. Should probably drop it.
	appVersion = versionInfo.GetVersion()

	// Setup Kingpin flags
	// Set the application version number
	app.Version(appVersion)
	// Allow -h as well as --help
	app.HelpFlag.Short('h')

	app.Flag("apiPort", "The port the API server should listen on. Can be set via HTTP_REC_PROXY_API_PORT.").Short('A').Default("8443").Envar("HTTP_REC_PROXY_API_PORT").IntVar(&apiPort)
	app.Flag("forwardPort", "The port the server should listen on for forwarded requests. Can be set via HTTP_REC_PROXY_FORWARD_PORT.").Short('F').Default("9443").Envar("HTTP_REC_PROXY_FORWARD_PORT").IntVar(&forwardPort)

	// Logging flags
	//app.Flag("log-utc", "Timestamp lug.Log messages in UTC instead of local time. Can be set via HTTP_REC_PROXY_LOG_UTC").Default("false").Envar("HTTP_REC_PROXY_LOG_UTC").BoolVar(&logUTC)
	//app.Flag("logLevel", "The level of lug.Logging to use. Can be set via HTTP_REC_PROXY_LOG_LEVEL").Short('l').Default("Error").Envar("HTTP_REC_PROXY_LOG_LEVEL").EnumVar(&logLevel, "None",
	//	"Error",
	//	"Warn",
	//	"Info",
	//	"Debug",
	//	"All")
	//app.Flag("logformat", "What output format to use. Can be set via HTTP_REC_PROXY_LOG_FMT").Short('f').Default("logfmt").Envar("HTTP_REC_PROXY_LOG_FMT").EnumVar(&logFormat, "logfmt", "json")

	// Now parse the flags
	kingpin.MustParse(app.Parse(os.Args[1:]))

	lug.InitLoggerFromYaml("lugConfig.yml")

	initCert()
}

// A main handler which lug.Logs when it was accessed and responds with "Sample App"
func mainHandler(w http.ResponseWriter, r *http.Request) {
	lug.Warn("Message", "/ called at %s\n", time.Now())
	// Set CSS headers
	setCORS(w)

	// Respond with a 200
	w.WriteHeader(http.StatusOK)
	message := fmt.Sprintf("Nothing here, perhaps you meant <a href=\"/metrics\"")
	io.WriteString(w, message)
}


// Pulls in or creates the certificate and key.
func initCert() {
	//TODO: Override these variables with command line flags

	// Create objects for the API Cert and key
	// Attempt to fecth the private key and certificate from environment variables.
	apiKeyPEMStr := os.Getenv("HTTP_REC_PROXY_PRIVATE_KEY")
	apiCertPEMStr := os.Getenv("HTTP_REC_PROXY_CERTIFICATE")

	// If either of those are empty
	if apiKeyPEMStr == "" || apiCertPEMStr == "" {
		if apiKeyPEMStr == "" {
			lug.Warn("Message", "Failed to find API key")
		}
		if apiCertPEMStr == "" {
			lug.Warn("Message", "Failed to find API Certificate")
		}
		lug.Warn("Message", "Generating key and self-signed certificate for API server")
		// Generate a default key and cert
		privKey, pubKey := genKeyPair(2048)

		apiKeyPEM = pem.EncodeToMemory(&pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(privKey)})
		apiCertPEM = genCert(privKey, pubKey)
	} else {
		lug.Warn("Message", "API Certificate and key found")
		pemStr, err := base64.StdEncoding.DecodeString(apiKeyPEMStr)
		if err != nil {
			lug.Error("Message", "Failed to decode PEM key", "Error", err)
		}
		certStr, err := base64.StdEncoding.DecodeString(apiCertPEMStr)
		if err != nil {
			lug.Error("Message", "Failed to decode PEM cert", "Error", err)
		}

		apiKeyPEM = []byte(pemStr)
		apiCertPEM = []byte(certStr)
	}

	// Create objects for the forwarding Cert and key
	// Attempt to fecth the private key and certificate from environment variables.
	fwdKeyPEMStr := os.Getenv("HTTP_REC_PROXY_FWD_PRIVATE_KEY")
	fwdCertPEMStr := os.Getenv("HTTP_REC_PROXY_FWD_CERTIFICATE")

	// If either of those are empty
	if fwdKeyPEMStr == "" || fwdCertPEMStr == "" {
		if fwdKeyPEMStr == "" {
			lug.Warn("Message", "Failed to find key")
		}
		if fwdCertPEMStr == "" {
			lug.Warn("Message", "Failed to find Certificate")
		}
		lug.Warn("Message", "Falling back to API cert/key pair")

		fwdKeyPEM = apiKeyPEM
		fwdCertPEM = apiCertPEM

		//TODO: Make dynamically generating these a command line option
		//lug.Warn("Message", "Generating key and self-signed certificate")
		//// Generate a default key and cert
		//privKey, pubKey := genKeyPair(2048)
		//
		//apiKeyPEM = pem.EncodeToMemory(&pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(privKey)})
		//apiCertPEM = genCert(privKey, pubKey)
	} else {
		lug.Warn("Message", "Certificate and key found")
		pemStr, err := base64.StdEncoding.DecodeString(fwdKeyPEMStr)
		if err != nil {
			lug.Error("Message", "Failed", "Error", err)
		}
		certStr, err := base64.StdEncoding.DecodeString(fwdCertPEMStr)
		if err != nil {
			lug.Error("Message", "Failed", "Error", err)
		}

		fwdKeyPEM = []byte(pemStr)
		fwdCertPEM = []byte(certStr)
	}

}

// Logs all requests then forwards them on to their originally intended destination
func forwardHandler(w http.ResponseWriter, r *http.Request) {

	lug.Info("Forward", "/ called at %s\n", time.Now())

	//hostName := r.Header.Get("Host")
	hostName := r.Host

	httpClient := &http.Client{
		// Uncomment this to ignore TLS verification when running on your laptop if you are troubled by certificate signed by unknown authority errors.
		//Transport:
		//&http.Transport{
		//	//Proxy: http.ProxyURL(proxyurl),
		//	TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		//},
	}

	url := fmt.Sprintf("https://%s%s", hostName, r.URL)

	lug.Debug("Message", "Composed URL", "Host", hostName, "Req URI", r.URL, "Composed URL result", url)

	defer r.Body.Close()

	reqBodyBytes, _ := ioutil.ReadAll(r.Body)

	reqBodyString := string(reqBodyBytes)

	lug.Info("Message", "Request recieved", "Body", reqBodyString, "Header", fmt.Sprintf("%v", r.Header))

	lug.Debug("Message", "Forwarding request", "URL", url)
	req, _ := http.NewRequest(r.Method, url, r.Body)

	// Actually perform the request.
	resp, err := httpClient.Do(req)

	if err != nil {
		lug.Error("Message", "Error performing request", "Error", err)
	}


	defer resp.Body.Close()

	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	bodyString := string(bodyBytes)

	lug.Info("Message", "Response recieved", "Body", bodyString, "Header", fmt.Sprintf("%v", resp.Header))


	w.WriteHeader(resp.StatusCode)
	for key, value := range resp.Header {
		for _, val2 := range value {
			w.Header().Add(key, val2)
		}
	}


	io.WriteString(w, bodyString)
	//message := fmt.Sprintf("Nothing here, perhaps you meant <a href=\"/webhook\"")
	//io.WriteString(w, message)
}



// Creates and returns reference a gorillamux router
func getRouter() *mux.Router {

	router := mux.NewRouter().StrictSlash(true)

	router.HandleFunc("/", mainHandler).Methods("GET")
	router.HandleFunc("/health", healthHandler)

	// Not sure how to enable metrics when I'm doing them the way I'm doing them.
	router.Handle("/metrics", promhttp.Handler())

	return router
}

// Creates and returns reference a gorillamux router
func getFwdRouter() *mux.Router {

	router := mux.NewRouter()//.StrictSlash(true)

	router.HandleFunc("/{reqPath}", forwardHandler)
	router.HandleFunc("/", forwardHandler)


	return router
}

// Generates a server and a listener for the specified configuration
func makeServer(key []byte, cert []byte, listenport int, routerFunc func() *mux.Router) (net.Listener, http.Server){
	// Configure TLS
	cer, err := tls.X509KeyPair(cert, key)
	if err != nil {
		lug.Error("Message", "Failed", "Error", err)
	}
	config := &tls.Config{
		MinVersion: tls.VersionTLS12,
		Certificates: []tls.Certificate{cer},
	}
	listener, err := tls.Listen("tcp", fmt.Sprintf(":%d", listenport), config)
	if err != nil {
		lug.Error("Message", "Failed to create listener", "Port", listenport, "Error", err)
	}

	server := http.Server{
		Handler:		routerFunc(),
		TLSConfig:		config,
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
	}

	return listener, server
}

func main() {
	// Initialize the prometheus counters and the /metrics path
	metricsArray = NewVerifyMetrics()
	metricsArray.RegisterAllMetrics()

	//// Configure TLS
	//cer, err := tls.X509KeyPair(apiCertPEM, apiKeyPEM)
	//if err != nil {
	//	lug.Error("Message", "Failed", "Error", err)
	//}
	//config := &tls.Config{
	//	MinVersion: tls.VersionTLS12,
	//	Certificates: []tls.Certificate{cer},
	//}
	//listener, err := tls.Listen("tcp", fmt.Sprintf(":%d", apiPort), config)
	//if err != nil {
	//	lug.Error("Message", "Failed to create listener", "Port", apiPort, "Error", err)
	//}
	//
	//server := http.Server{
	//	Handler:		getRouter(),
	//	TLSConfig:		config,
	//	WriteTimeout: time.Second * 15,
	//	ReadTimeout:  time.Second * 15,
	//	IdleTimeout:  time.Second * 60,
	//}

	// Create the forwarder server
	fwdListener, fwdServer := makeServer(fwdKeyPEM, fwdCertPEM, forwardPort, getFwdRouter)

	//TODO: Do something smarter here (will have to understand go's native context a bit more for that.)
	var fwdCancel context.CancelFunc
	var fwdCtx context.Context
	fwdCtx, fwdCancel = context.WithCancel(context.Background())
	//_, fwdCancel = context.WithCancel(context.Background())
	defer fwdCancel() // cancel when we are finished

	var fwdError error

	go func() {
		lug.Warn("Message", "Starting forwarding Server", "Port", forwardPort)
		//TODO: There must be some reason I'm not using lug.Error here?
		//fmt.Printf("Failed with error %v", fwdServer.Serve(fwdListener))
		fwdError = fwdServer.Serve(fwdListener)
		defer fwdServer.Shutdown(fwdCtx)
		//DNSerr = DNSServer.ListenAndServe()
		//defer DNSServer.Shutdown()
	}()

	if fwdError != nil {
		lug.Error("Message", "Failed to start forwarding server", "Error", fwdError.Error())
	}

	// Create the API server
	apiListener, apiServer := makeServer(apiKeyPEM, apiCertPEM, apiPort, getRouter)

	////TODO: Do something smarter here (will have to understand go's native context a bit more for that.)
	var cancel context.CancelFunc
	var ctx context.Context
	ctx, cancel = context.WithCancel(context.Background())
	defer cancel() // cancel when we are finished

	//http.ServeTLS(listener, router, getMux(versionInfo))
	lug.Warn("Message", "Application startup", "Version", BaseVersion, "APIPort", apiPort, "ForwardPort", forwardPort)

	defer apiServer.Shutdown(ctx)
	//TODO: There must be some reason I'm not using lug.Error here?
	fmt.Printf("Failed with error %v", apiServer.Serve(apiListener))

}